import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import 'package:get/get.dart';
import 'package:herai/app/controllers/auth_controller.dart';
import 'package:herai/app/data/data_source/shared_pref_secure_storage.dart';
import 'package:herai/app/data/network/herai_api.dart';

import 'package:herai/app/utils/loading.dart';
import 'package:herai/app/utils/log.dart';

import 'app/routes/app_pages.dart';
import 'app/utils/translation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      // options: DefaultFirebaseOptions.currentPlatform,
      );

  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  ); // To turn off landscape mode
  await Future.delayed(Duration(seconds: 3));
  int _onboarding =
      await SharedPrefSecureStorage.getInstance()?.getPrefOnBoarding() ?? 0;
  print(_onboarding);
  runApp(HerAiApp(_onboarding));
}

class HerAiApp extends StatelessWidget {
  final authController = Get.put(AuthController(), permanent: true);
  final api = Get.put(HerAiApi(), permanent: true);
  int _onboarding = 0;

  HerAiApp(this._onboarding);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: authController.streamAuthStatus,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          // authController.profile.value.username =
          //     snapshot.data!.displayName ?? '';
          // authController.profile.value.photoURL = snapshot.data!.photoURL ?? '';
          // authController.profile.value.userEmail = snapshot.data!.email ?? '';
          // authController.profile.value.username =
          //     snapshot.data!.displayName ?? '';

          return GetMaterialApp(
            debugShowCheckedModeBanner: false,
            translations: AppTranlation(),
            locale: Locale(authController.languange.value),
            //Locale('id'),
            title: "Application",
            initialRoute: //Routes.PILIH_SAMPAH,
                _onboarding == 0
                    ? Routes.ONBOARDING_PAGE
                    : snapshot.data != null && snapshot.data!.emailVerified
                        ? Routes.HOME
                        : Routes.LOGIN_PAGE,
            getPages: AppPages.routes,
            theme: ThemeData(
              appBarTheme: AppBarTheme(
                color: Color(0xff50C446),
              ),
              primarySwatch: Colors.green,
              primaryColor: Color(0xff50C446),
            ),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}
