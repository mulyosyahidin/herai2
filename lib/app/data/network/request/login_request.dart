import 'dart:convert';

LoginRequest loginFromMap(String str) => LoginRequest.fromMap(json.decode(str));

String loginToMap(LoginRequest data) => json.encode(data.toMap());

class LoginRequest {
  LoginRequest({
    required this.email,
    required this.password,
  });

  String email;
  String password;

  factory LoginRequest.fromMap(Map<String, dynamic> json) => LoginRequest(
    email: json["email"] == null ? null : json["email"],
    password: json["password"] == null ? null : json["password"],
  );

  Map<String, dynamic> toMap() => {
    "email": email == null ? null : email,
    "password": password == null ? null : password,
  };
}
