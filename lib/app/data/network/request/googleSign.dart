import 'dart:convert';

GoogleAuthLogin googleAuthLoginFromMap(String str) =>
    GoogleAuthLogin.fromMap(json.decode(str));

String googleAuthLoginToMap(GoogleAuthLogin data) => json.encode(data.toMap());

class GoogleAuthLogin {
  GoogleAuthLogin({
    required this.token,
  });

  String token;

  factory GoogleAuthLogin.fromMap(Map<String, dynamic> json) => GoogleAuthLogin(
        token: json["token"] == null ? '' : json["token"],
      );

  Map<String, dynamic> toMap() => {
        "token": token == null ? '' : token,
      };
}
