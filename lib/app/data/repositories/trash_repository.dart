import 'dart:convert';

import 'package:herai/app/config/global.dart';
import 'package:herai/app/data/entities/model_sampah.dart';
import 'package:herai/app/data/network/herai_api.dart';
import 'package:herai/app/data/network/response/base_list_response.dart';
import 'package:herai/app/data/network/response/base_response.dart';

import 'dart:async';

import 'package:herai/app/utils/log.dart';

class TrashRepository {
  late HerAiApi _api;

  TrashRepository() {
    _api = HerAiApi();
  }

  Future<List<DataSampah>> getTrash() async {
    final response = await _api.get(Urls.getTrash);

    final List<DataSampah> data =
        baseListResponseFromMap<DataSampah>(response, (p0) {
      Log.colorGreen('data_P0 : ${jsonEncode(p0)}');
      return DataSampah.fromMap(p0);
    }).data;

    return data;
  }
}
