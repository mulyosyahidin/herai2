import 'package:get/get.dart';
import 'package:herai/app/data/data_source/shared_pref_secure_storage.dart';
import 'package:herai/app/data/network/herai_api.dart';
import 'package:herai/app/data/network/request/googleSign.dart';
import 'package:herai/app/data/network/request/register_email_request.dart';
import 'package:herai/app/data/network/response/base_response.dart';
import 'package:herai/app/data/network/response/login_response.dart';

import '../entities/user.dart';
import '../network/request/login_request.dart';

abstract class AuthEndpoints {
  static final loginEmail = "/api/auth/login";
  static final registerEmail = "/api/auth/register";
  static final getUserData = "/api/user";
  static final googleAccount = "/api/auth/google";
}

class AuthRepository extends GetxController {
  Rx<UserProfile> user = UserProfile.empty().obs;
  late HerAiApi _api;
  AuthRepository() {
    _api = HerAiApi();
  }

  Future<UserProfile?> loginEmail(LoginRequest loginRequest) async {
    final response =
        await _api.post(AuthEndpoints.loginEmail, loginToMap(loginRequest));
    LoginResponse data = baseResponseFromMap<LoginResponse>(
        response, (p0) => LoginResponse.fromMap(p0)).data;
    var token = data.token;
    SharedPrefSecureStorage.getInstance()!.saveToken(token);
    return data.user;
  }

  Future<UserProfile?> googleLogin(GoogleAuthLogin authToken) async {
    final response = await _api.post(
        AuthEndpoints.googleAccount, googleAuthLoginToMap(authToken));
    LoginResponse data = baseResponseFromMap<LoginResponse>(
        response, (p0) => LoginResponse.fromMap(p0)).data;
    var token = data.token;
    SharedPrefSecureStorage.getInstance()!.saveToken(token);
    return data.user;
  }

  Future<UserProfile?> registerEmail(
      RegisterEmailRequest registerEmailRequest) async {
    final response = await _api.post(AuthEndpoints.registerEmail,
        registerEmailRequestToMap(registerEmailRequest));
    LoginResponse data = baseResponseFromMap<LoginResponse>(
        response, (p0) => LoginResponse.fromMap(p0)).data;
    var token = data.token;
    SharedPrefSecureStorage.getInstance()!.saveToken(token);
    return data.user;
  }

  Future<UserProfile> getUserData() async {
    final response = await _api.get(AuthEndpoints.getUserData);

    user.value = baseResponseFromMap<UserProfile>(
        response, (p0) => UserProfile.fromMap(p0)).data;
    return user.value;
  }
}
