class PickupRequest {
  PickupRequest({
    required this.pointId,
    required this.address,
    required this.coordinate,
    required this.items,
  });

  String pointId;
  String address;
  String coordinate;
  List<int> items;

  factory PickupRequest.fromMap(Map<String, dynamic> json) => PickupRequest(
    pointId: json["point_id"] == null ? 0 : json["point_id"],
    address: json["address"] == null ? "" : json["address"],
    coordinate: json["coordinate"] == "" ? null : json["coordinate"],
    items: json["items"] == null ? [] : List<int>.from(json["items"].map((x) => x)),
  );

  Map<String, dynamic> toMap() => {
    "point_id": pointId == null ? null : pointId,
    "address": address == null ? null : address,
    "coordinate": coordinate == null ? null : coordinate,
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x)),
  };
}
