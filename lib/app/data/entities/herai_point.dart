// To parse this JSON data, do
//
//     final herAiPoint = herAiPointFromJson(jsonString);

import 'dart:convert';

import 'package:meta/meta.dart';

List<HerAiPoint> listHerAiPointFromJson(List<Map<String, dynamic>> json) =>
    List<HerAiPoint>.from(json.map((e) => HerAiPoint.fromJson(e)));

String listHerAiPointToJson(List<HerAiPoint> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

HerAiPoint herAiPointFromJson(String str) =>
    HerAiPoint.fromJson(json.decode(str));

String herAiPointToJson(HerAiPoint data) => json.encode(data.toJson());

class HerAiPoint {
  HerAiPoint({
    required this.id,
    required this.name,
    required this.address,
    required this.phoneNumber,
    required this.coordinate,
    required this.latitude,
    required this.longitude,
    required this.createdAt,
    required this.updatedAt,
    required this.deletedAt,
  });

  final int id;
  final String name;
  final String address;
  final String phoneNumber;
  final double latitude;
  final double longitude;
  final String coordinate;
  final DateTime createdAt;
  final DateTime updatedAt;
  final dynamic deletedAt;

  factory HerAiPoint.fromJson(Map<String, dynamic> json) {
    String coordinate = json["coordinate"];
    List<double> coordinateDouble =
        coordinate.split(",").map((e) => double.parse(e)).toList();

    return HerAiPoint(
      id: json["id"] == null ? null : json["id"],
      name: json["name"] == null ? null : json["name"],
      address: json["address"] == null ? null : json["address"],
      phoneNumber: json["phone_number"] == null ? "" : json["phone_number"],
      coordinate: json["coordinate"] == null ? null : json["coordinate"],
      latitude: coordinateDouble[0],
      longitude: coordinateDouble[1],
      createdAt: json["created_at"] == null
          ? DateTime.now()
          : DateTime.parse(json["created_at"]),
      updatedAt: json["updated_at"] == null
          ? DateTime.now()
          : DateTime.parse(json["updated_at"]),
      deletedAt: json["deleted_at"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "coordinate": coordinate == null ? null : coordinate,
        "created_at": createdAt == null ? DateTime.now() : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? DateTime.now() : updatedAt.toIso8601String(),
        "deleted_at": deletedAt,
      };

  factory HerAiPoint.empty() => HerAiPoint(
      id: 1,
      name: "name",
      address: "address",
      phoneNumber: "phoneNumber",
      coordinate: "1,1",
      latitude: 0.0,
      longitude: 0.0,
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
      deletedAt: "deletedAt");
}
