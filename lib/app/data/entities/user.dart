class UserProfile {
  UserProfile({
    required this.id,
    required this.name,
    required this.email,
    required this.emailVerifiedAt,
    required this.role,
    required this.provider,
    required this.createdAt,
    required this.updatedAt,
    required this.point,
    required this.totalTransaction,
    required this.media,
  });

  int id;
  String name;
  String email;
  DateTime emailVerifiedAt;
  String role;
  String provider;
  DateTime createdAt;
  DateTime updatedAt;
  int point;
  int totalTransaction;
  List<Media> media;

  factory UserProfile.empty() => UserProfile(
      id: 0,
      name: '',
      email: '',
      emailVerifiedAt: DateTime.now(),
      role: '',
      provider: '',
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
      point: 0,
      totalTransaction: 0,
      media: []);

  factory UserProfile.fromMap(Map<String, dynamic> json) => UserProfile(
        id: json["id"] == null ? 0 : json["id"],
        name: json["name"] == null ? '' : json["name"],
        email: json["email"] == null ? '' : json["email"],
        emailVerifiedAt: json["email_verified_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["email_verified_at"]),
        role: json["role"] == null ? '' : json["role"],
        provider: json["provider"] == null ? '' : json["provider"],
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
        point: json["point"] == null ? 0 : json["point"],
        totalTransaction:
            json["total_transaction"] == null ? 0 : json["total_transaction"],
        media: json["media"] == null
            ? [Media.empty()]
            : List<Media>.from(json["media"].map((x) => Media.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "email": email,
        "email_verified_at": emailVerifiedAt,
        "role": role,
        "provider": provider,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "point": point,
        "total_transaction": totalTransaction,
        "media": List<dynamic>.from(media.map((x) => x.toMap())),
      };
}

class Media {
  Media({
    required this.id,
    required this.modelType,
    required this.modelId,
    required this.uuid,
    required this.collectionName,
    required this.name,
    required this.fileName,
    required this.mimeType,
    required this.disk,
    required this.conversionsDisk,
    required this.size,
    required this.manipulations,
    required this.customProperties,
    required this.generatedConversions,
    required this.responsiveImages,
    required this.orderColumn,
    required this.createdAt,
    required this.updatedAt,
    required this.originalUrl,
    required this.previewUrl,
  });

  int id;
  String modelType;
  String modelId;
  String uuid;
  String collectionName;
  String name;
  String fileName;
  String mimeType;
  String disk;
  String conversionsDisk;
  String size;
  List<dynamic> manipulations;
  List<dynamic> customProperties;
  List<dynamic> generatedConversions;
  List<dynamic> responsiveImages;
  String orderColumn;
  DateTime createdAt;
  DateTime updatedAt;
  String originalUrl;
  String previewUrl;

  factory Media.empty() => Media(
      id: 0,
      modelType: '',
      modelId: '',
      uuid: '',
      collectionName: '',
      name: '',
      fileName: '',
      mimeType: '',
      disk: '',
      conversionsDisk: '',
      size: '',
      manipulations: [],
      customProperties: [],
      generatedConversions: [],
      responsiveImages: [],
      orderColumn: '',
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
      originalUrl: '',
      previewUrl: '');

  factory Media.fromMap(Map<String, dynamic> json) => Media(
        id: json["id"] == null ? null : json["id"],
        modelType: json["model_type"] == null ? null : json["model_type"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        collectionName:
            json["collection_name"] == null ? null : json["collection_name"],
        name: json["name"] == null ? null : json["name"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        mimeType: json["mime_type"] == null ? null : json["mime_type"],
        disk: json["disk"] == null ? null : json["disk"],
        conversionsDisk:
            json["conversions_disk"] == null ? null : json["conversions_disk"],
        size: json["size"] == null ? null : json["size"],
        manipulations: json["manipulations"] == null
            ? []
            : List<dynamic>.from(json["manipulations"].map((x) => x)),
        customProperties: json["custom_properties"] == null
            ? []
            : List<dynamic>.from(json["custom_properties"].map((x) => x)),
        generatedConversions: json["generated_conversions"] == null
            ? []
            : List<dynamic>.from(json["generated_conversions"].map((x) => x)),
        responsiveImages: json["responsive_images"] == null
            ? []
            : List<dynamic>.from(json["responsive_images"].map((x) => x)),
        orderColumn: json["order_column"] == null ? null : json["order_column"],
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
        originalUrl: json["original_url"] == null ? null : json["original_url"],
        previewUrl: json["preview_url"] == null ? null : json["preview_url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "model_type": modelType,
        "model_id": modelId,
        "uuid": uuid,
        "collection_name": collectionName,
        "name": name,
        "file_name": fileName,
        "mime_type": mimeType,
        "disk": disk,
        "conversions_disk": conversionsDisk,
        "size": size,
        "manipulations": manipulations == null
            ? []
            : List<dynamic>.from(manipulations.map((x) => x)),
        "custom_properties": customProperties == null
            ? []
            : List<dynamic>.from(customProperties.map((x) => x)),
        "generated_conversions": generatedConversions == null
            ? []
            : List<dynamic>.from(generatedConversions.map((x) => x)),
        "responsive_images": responsiveImages == null
            ? []
            : List<dynamic>.from(responsiveImages.map((x) => x)),
        "order_column": orderColumn,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "original_url": originalUrl,
        "preview_url": previewUrl,
      };
}
