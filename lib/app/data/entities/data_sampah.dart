const Map<String, dynamic> data = {
  "data": [
    {
      "nama": "PLASTIK",
      "jenis": [
        {"type": "PP Bening (Toko)", "harga": 2800},
        {"type": "PP Bening Kotor", "harga": 1500},
        {"type": "PP Sablon", "harga": 600},
        {"type": "PP Blok (Bungkus Mie Instant)", "harga": 400},
        {"type": "Kresek / Sunlight Kecil", "harga": 400},
        {"type": "PE Bersih", "harga": 1250},
        {"type": "PE Kotor", "harga": 750},
        {"type": "Plastik Sunlight Besar", "harga": 700},
        {"type": "PE Slopan (Bimoli)", "harga": 700},
        {"type": "PP Aqua Gelas Bersih", "harga": 6500},
        {"type": "PP Gelas Aqua Kotor", "harga": 4000},
        {"type": "Gelas Ale2", "harga": 2700},
        {"type": "PET Botol Bening Bersih", "harga": 3500},
        {"type": "PET Botol Bening Kotor", "harga": 1800},
        {"type": "PET Botol Warna Bersih", "harga": 2000},
        {"type": "PET Botol Warna Kotor", "harga": 1500},
        {"type": "PP Bak Warna", "harga": 2900},
        {"type": "PP Bak Hitam", "harga": 1400},
        {"type": "HDPE Blowing", "harga": 3200},
        {"type": "Dirigen", "harga": 4000},
        {"type": "Kulit Kabel", "harga": 1200},
        {"type": "Paralon", "harga": 600},
        {"type": "LDPE Infus", "harga": 5500},
        {"type": "Karpet /Talang Plastik/Jas Hujan", "harga": 700},
        {"type": "Tutup Aqua Galon", "harga": 3000},
        {"type": "Tutup Botol Warna", "harga": 2600},
        {"type": "Tali PET", "harga": 500},
        {"type": "Selang Air", "harga": 1250},
        {"type": "Glangsi Utuh 50kg/biji", "harga": 400},
        {"type": "Glangsi Utuh 25kg/biji", "harga": 300},
        {"type": "Glangsi Utuh 10kg/biji", "harga": 250},
        {"type": "Glangsi Rusak", "harga": 400},
        {"type": "Plastik Keras", "harga": 600},
        {"type": "Plastik Keras Bening", "harga": 3700},
        {"type": "CD/DVD/MP3/Kaset PS", "harga": 3200},
        {"type": "Galon PC rusak utuh/tidak putus/biji", "harga": 3700},
        {"type": "Aki kecil", "harga": 9000},
        {"type": "AKI Besar Tanggung", "harga": 16000},
        {"type": "AKI Besar Mobil 50 Jet", "harga": 25000}
      ]
    },
    {
      "nama": "KERTAS",
      "jenis": [
        {"type": "Buku Tulis", "harga": 2000},
        {"type": "HVS", "harga": 2000},
        {"type": "Koran", "harga": 3000},
        {"type": "Kertas Semen", "harga": 3000},
        {"type": "Majalah/Duplek", "harga": 500},
        {"type": "Karton/ Kardus Bagus", "harga": 1500},
        {"type": "Kertas Campur", "harga": 1000},
        {"type": "Kertas Buram/CD", "harga": 1300}
      ]
    },
    {
      "nama": "LOGAM",
      "jenis": [
        {"type": "Seng Kaleng", "harga": 1200},
        {"type": "Seng Biasa", "harga": 400},
        {"type": "Besi Super", "harga": 2300},
        {"type": "Besi Biasa/Monel Maspion", "harga": 1300},
        {"type": "Slender Cop/Seker", "harga": 12500},
        {"type": "Antena/ Panci/Wajan", "harga": 10000},
        {"type": "Kaleng Alumunium", "harga": 9000},
        {"type": "Plat", "harga": 11500},
        {"type": "Siku", "harga": 14500},
        {"type": "Tutup Botol Alumunium", "harga": 3500},
        {"type": "Perunggu", "harga": 6500},
        {"type": "Stenlis Monel", "harga": 14000},
        {"type": "Kuningan", "harga": 30000},
        {"type": "Tembaga Biasa", "harga": 40000},
        {"type": "Tembaga Super", "harga": 42500}
      ]
    },
    {
      "nama": "BOTOL DAN KACA",
      "jenis": [
        {"type": "Botol Kecil/biji", "harga": 100},
        {"type": "Botol Marjan/Biji", "harga": 100},
        {"type": "Botol Orson/Biji", "harga": 100},
        {"type": "Botol Kecap/Saos Besar/biji", "harga": 550},
        {"type": "Botol Bensin/biji", "harga": 1000},
        {"type": "Botol Bir/biji", "price": 600},
        {"type": "Botol Coca Cola/Sprite/Kg", "price": 75},
        {"type": "Beling/Kg", "price": 75}
      ]
    }
  ]
};
