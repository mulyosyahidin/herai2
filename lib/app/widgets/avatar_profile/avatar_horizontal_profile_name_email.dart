import 'package:flutter/material.dart';

import '../../data/entities/user.dart';
import '../../resource/colors_data.dart';

class AvatarProfileNameEmail extends StatelessWidget {
  const AvatarProfileNameEmail({
    Key? key,
    required this.user,
  }) : super(key: key);

  final UserProfile user;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 24,
        right: 24,
        top: 12,
        bottom: 40,
      ),
      decoration: BoxDecoration(
        color: Color(0xff50C346),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          user.media.isEmpty
              ? CircleAvatar(
                  backgroundImage:
                      AssetImage('assets/images/blank_profile.png'),
                  backgroundColor: Color(0xffD9D9D9),
                  minRadius: 24,
                )
              : CircleAvatar(
                  backgroundImage: NetworkImage(user.media.first.originalUrl),
                  backgroundColor: Color(0xffD9D9D9),
                  minRadius: 24,
                ),
          SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${user.name}',
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                '${user.email}',
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ButtonCardTransaction extends StatelessWidget {
  final String iconAssets;
  final String textTodo;
  const ButtonCardTransaction({
    Key? key,
    required this.iconAssets,
    required this.textTodo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      width: 150,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: ColorsCustom.primaryColor.midOrange),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset(
            iconAssets,
            width: 42,
            height: 42,
            fit: BoxFit.cover,
          ),
          SizedBox(
            height: 15,
          ),
          SizedBox(
            width: 120,
            child: Text(
              textTodo,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.clip,
            ),
          )
        ],
      ),
    );
  }
}
