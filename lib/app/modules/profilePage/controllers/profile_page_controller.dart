import 'package:get/get.dart';
import 'package:herai/app/data/entities/user.dart';

class ProfilePageController extends GetxController {
  Rx<UserProfile> user = UserProfile.empty().obs;
  @override
  void onInit() {
    super.onInit();
    user.value = Get.arguments['user'];

    print('profile_page here');
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}

// class User {
//   final String name;
//   final String email;
  
// }
