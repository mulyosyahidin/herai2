import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:herai/app/resource/colors_data.dart';

import '../../../widgets/avatar_profile/avatar_horizontal_profile_name_email.dart';
import '../controllers/profile_page_controller.dart';

class ProfilePageView extends GetView<ProfilePageController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leadingWidth: 60,
        title: Text('profile_profil_pengguna'.tr),
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Color(0xfB50C346),
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.light,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              textDirection: TextDirection.rtl,
              fit: StackFit.loose,
              clipBehavior: Clip.none,
              children: [
                Obx(() => AvatarProfileNameEmail(
                      user: controller.user.value,
                    )),
                Positioned(
                  bottom: -70,
                  child: Container(
                    padding: EdgeInsets.all(20),
                    width: Get.width - 80,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 4,
                            color: Color(0xff000000).withOpacity(.24),
                            offset: Offset(0, 4))
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Obx(() => Text(
                                  '${controller.user.value.point}',
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: ColorsCustom.primaryColor.midOrange,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Text('HerAi Coins'),
                          ],
                        ),
                        Container(
                          width: 2,
                          height: 50,
                          color: ColorsCustom.darkGrey30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Obx(() => Text(
                                  '${controller.user.value.totalTransaction}',
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: ColorsCustom.primaryColor.midOrange,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            SizedBox(height: 10),
                            Text('profile_total_transaksi'.tr),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 100,
            ),
            IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonCardTransaction(
                    iconAssets: 'assets/icons/mobile_wallet.png',
                    textTodo: 'profile_transfer_ke_dompet_digital'.tr,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  ButtonCardTransaction(
                      iconAssets: 'assets/icons/bank_transfer.png',
                      textTodo: 'profile_transfer_ke_bank'.tr),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
