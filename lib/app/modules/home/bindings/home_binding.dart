import 'package:get/get.dart';
import 'package:herai/app/data/repositories/auth_repository.dart';

import '../../../data/usecase/classify_image_case.dart';
import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<AuthRepository>(
      () => AuthRepository(),
    );
  }
}
