import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:herai/app/data/entities/user.dart';
import 'package:herai/app/data/repositories/auth_repository.dart';
import 'package:herai/app/routes/app_pages.dart';
import 'package:herai/app/utils/log.dart';

class FinishPageController extends GetxController {
  AuthRepository authRepository = Get.find<AuthRepository>();
  UserProfile user = UserProfile.empty();

  RxString message = "success_pengiriman_akan_diteruskan_ke_pengepul_silahkan_cek_status_di_riwayat_transaksi".tr.obs;

  @override
  void onInit() {
    super.onInit();
    message.value = Get.arguments["message"];
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> willPop(BuildContext context) async {
    // FocusScope.of(context).unfocus();
    user = await getUser();
    Get.offAllNamed(Routes.HOME, arguments: {'user': user});

    return true;
  }

  void gotoHistoryTransaction() async {
    user = await getUser();

    Get.toNamed(Routes.HISTORY_TRANSACTION_PAGE, arguments: {'user': user});
  }

  void gotoHome() async {
    user = await getUser();
    Get.offAllNamed(Routes.HOME, arguments: {'user': user});
  }

  Future<UserProfile> getUser() async {
    try {
      if (user.name.isEmpty) user = await authRepository.getUserData();

      Log.colorGreen('user : ${user.name}');
    } catch (e) {
      Log.info('INI ERROR NYA : $e');
    }
    return user;
  }
}
