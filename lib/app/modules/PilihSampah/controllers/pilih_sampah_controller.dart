import 'dart:io';

import 'package:get/get.dart';
import 'package:herai/app/data/entities/data_sampah.dart';
import 'package:herai/app/data/entities/model_sampah.dart';
import 'package:herai/app/data/network/request/add_item.dart';
import 'package:herai/app/data/repositories/cart_repository.dart';
import 'package:herai/app/data/repositories/trash_repository.dart';
import 'package:herai/app/routes/app_pages.dart';
import 'package:herai/app/utils/log.dart';

class PilihSampahController extends GetxController {
  RxList<DataSampah> dataSampah = <DataSampah>[].obs;

  Rx<DataSampah> selectedTrash = DataSampah.empty().obs;

  CartRepository _cartRepository = CartRepository();
  TrashRepository _trashRepository = TrashRepository();

  File? fileImage;

  @override
  void onInit() async {
    dataSampah.value = await getTrash();
    selectedTrash.value = dataSampah.value.first;
    Log.colorGreen('data_smapah : ${dataSampah.length}');

    String path = Get.arguments['imagePath'] ?? '';
    Log.colorGreen('path : ${path}');

    if (path.isNotEmpty) fileImage = File(path);

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<List<DataSampah>> getTrash() async {
    var data = await _trashRepository.getTrash();

    Log.colorGreen('cart_value : ${dataSampah.length}');

    return data;
  }

  void setSelectedKategori(DataSampah dataSampah) {
    selectedTrash.value = dataSampah;
    Log.colorGreen('list_jenis : ${dataSampah.trashs.map((e) => e.name)}');
  }

  void addWeight(Trash trashSelected) {
    trashSelected.weight++;
    selectedTrash.refresh();
  }

  void subWeight(Trash trashSelected) {
    if (trashSelected.weight > 0) trashSelected.weight--;
    selectedTrash.refresh();
  }

  void addToCart(AddItem addItem) async {
    Log.colorGreen('item : ${addItem.itemid}');
    final item = await _cartRepository.postAddItem(addItem);
    if (item) {
      Get.offNamed(
        Routes.CART_PAGE,
      );
    }
  }
}
