import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:herai/app/resource/colors_data.dart';
import 'package:herai/app/resource/styles.dart';
import 'package:herai/app/routes/app_pages.dart';
import 'package:herai/app/widgets/button_card/button_custom.dart';

import '../controllers/gmaps_page_controller.dart';

class GmapsPageView extends GetView<GmapsPageController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('GmapsPageView'),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // controller.currentLocation == null
            //     ? Container(
            //         margin: EdgeInsets.symmetric(vertical: 150),
            //         child: Center(
            //           child: CircularProgressIndicator(),
            //         ),
            //       )
            //     :

            Obx(
              () => controller.isloading.isTrue
                  ? Container(
                      height: Get.height * .6,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Container(
                      width: Get.width,
                      height: Get.height * .6,
                      child: GoogleMap(
                        myLocationButtonEnabled: true,
                        // myLocationEnabled: true,
                        zoomGesturesEnabled: true,
                        initialCameraPosition: CameraPosition(
                          target: controller.originLocation.value,
                          zoom: 14.5,
                        ),
                        polylines: //controller.poliLine,
                            {
                          Polyline(
                            polylineId: PolylineId('route'),
                            points: List<LatLng>.from(
                                controller.polylineCoordinates.value),
                            color: ColorsCustom.primaryColor.green,
                            width: 4,
                          )
                        },
                        markers: {
                          Marker(
                            markerId: MarkerId('Origin'),
                            position: LatLng(
                              controller.originLocation.value.latitude,
                              controller.originLocation.value.longitude,
                            ),
                            draggable: true,
                            onDragEnd: (pin) {
                              controller.originLocation.value = pin;
                              controller.getPolyPoints();
                              print(
                                  'current_location : ${controller.originLocation.value.latitude}, long: ${controller.originLocation.value.longitude}');
                            },
                          ),
                          Marker(
                            markerId: MarkerId('Destination'),
                            position: controller.destinationLocation.value,
                          ),
                        },
                        onLongPress: controller.addMarker,
                      ),
                    ),
            ),

            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 24),
              width: Get.width,
              height: Get.height * .4,
              decoration: BoxDecoration(
                color: Color(0xffFBF9F7),
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    height: 4,
                    width: Get.width * .25,
                    decoration: BoxDecoration(color: Colors.grey),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Obx(
                    () => GmapsInformation(
                      title_info: 'gmaps_titik_penjemputan',
                      info: controller.pickUpLocation.value,
                      // '${controller.originLocation.value.latitude}, ${controller.originLocation.value.longitude}')),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Obx(
                    () => GmapsInformation(
                        title_info: 'gmaps_herAi_poin',
                        info:
                            '${controller.nearestPoint.value.address}\n(${controller.destinationLocation.value.latitude}, ${controller.destinationLocation.value.longitude})'),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Obx(() => ButtonCustom(
                        width: Get.width,
                        text: 'gmaps_angkut',
                        color: ColorsCustom.primaryColor.green,
                        onTap: controller.isloading.isTrue
                            ? () {}
                            : () {
                                controller.finishlahpokonya();
                              },
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GmapsInformation extends StatelessWidget {
  String title_info;
  String info;
  GmapsInformation({
    Key? key,
    required this.title_info,
    required this.info,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title_info.tr,
          style: TextStyleHeading.textH6XSmall.copyWith(
              fontWeight: FontWeight.w700,
              color: ColorsCustom.primaryColor.green),
          // textAlign: TextAlign.left,
        ),
        Container(
          margin: EdgeInsets.only(top: 8),
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
          width: Get.width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(
              4,
            ),
          ),
          child: Text(
            info.tr,
            style: TextStyleHeading.textH8SuperSmall.copyWith(
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }
}
